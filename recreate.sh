#!/usr/bin/env bash
# while read -r line; do echo "$line" | sed -E 's/^(\s*\S*){10}\s*//gm' | xargs realpath --relative-to=$1 -L -m | xargs mkdir -p; done < emptydirs.txt
BASE_DIR="$(realpath $1)"
while read -r line;
do
  DIR_PATH="$(echo "$line" | sed -E 's/^(\s*\S*){10}\s*\.//gm')"
  mkdir -p "$BASE_DIR$DIR_PATH"
done < $2
