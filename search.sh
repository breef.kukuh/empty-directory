#!/usr/bin/env bash
search_location=$1
output_file="/$(realpath --relative-to=/ -L -m $2)"
cd $search_location && find . -type d -empty -ls | cat > $output_file
